<?php

namespace Paroleen\Twilio;

use Twilio\Rest\Client;

class TwilioWhatsApp {

    protected $client;

    public function __construct(Client $client) {
        $this->client = $client;
    }

    public function send(string $number, string $message) {
        return $this->client->messages->create('whatsapp:' . $number, [
            'from' => 'whatsapp:' . config('twilio.whatsapp_from'),
            'body' => $message
        ]);
    }
}
