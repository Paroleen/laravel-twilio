<?php

namespace Paroleen\Twilio\Facades;

use Illuminate\Support\Facades\Facade;

class WhatsApp extends Facade {

    public static function getFacadeAccessor() {
        return 'twilio_whatsapp';
    }
}
